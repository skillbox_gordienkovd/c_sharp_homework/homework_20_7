﻿using homework_20_7.Services;
using Microsoft.AspNetCore.Mvc;

namespace homework_20_7.Controllers;

public class AccountController: Controller
{
    private readonly AccountService _accountService = new AccountService();
    
    public IActionResult Index()
    {
        return View(AccountService.FindAll());
    }
    
    public IActionResult Details(long id)
    {
        return View(AccountService.FindById(id));
    }
}