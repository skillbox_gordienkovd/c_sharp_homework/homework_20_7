﻿using homework_20_7.Models;
using homework_20_7.Repository;

namespace homework_20_7.Services;

public class AccountService
{
    public static List<Account> FindAll()
    {
        using NotebookDbContext dbContext = new();
        
        return dbContext.Accounts.ToList();
    }

    public static Account FindById(long id)
    {
        using NotebookDbContext dbContext = new();
        
        return dbContext.Accounts.First(account => account.ID == id);
    }
}