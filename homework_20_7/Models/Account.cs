﻿namespace homework_20_7.Models;

public class Account
{
    public long ID { get; set; }
    public string Firstname { get; set; }
    public string Patronomyc { get; set; }
    public string Lastname { get; set; }
    public string Phone { get; set; }
    public string Address { get; set; }
}